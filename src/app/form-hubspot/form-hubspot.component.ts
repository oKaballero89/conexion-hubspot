import { HubspotCnnService } from './../service/hubspot-cnn.service';
import { Component, OnInit, NgModule } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators, FormGroupDirective, NgForm } from '@angular/forms';

@Component({
  selector: 'app-form-hubspot',
  templateUrl: './form-hubspot.component.html',
  styleUrls: ['./form-hubspot.component.css']
})
export class FormHubspotComponent implements OnInit {
  
  firstname: FormControl;
  lastname: FormControl;
  mobilephone: FormControl;
  email: FormControl;
  city: FormControl;
  phone: FormControl;
  cookieTracking:string;

  form: FormGroup;

  constructor(private hubspot: HubspotCnnService) { }

  ngOnInit() {
    this.cookieTracking =  this.hubspot.getCookie();

    this.form = new FormGroup({
      firstname: new FormControl(''),
      lastname: new FormControl(''),
      mobilephone: new FormControl(''),
      email: new FormControl(''),
      city: new FormControl(''),
      phone: new FormControl('')
    })
  }

  onSubmit(){
    let params = {
      firstname: this.form.controls['firstname'].value,
      lastname: this.form.controls['lastname'].value,
      mobilephone: this.form.controls['mobilephone'].value,
      email: this.form.controls['email'].value,
      city: this.form.controls['city'].value,
      phone: this.form.controls['phone'].value
    }

        if(this.cookieTracking!= undefined){
          let parametros_HS = {
            hutk: this.cookieTracking,
            ipAddress: "192.168.1.12", 
            pageUrl: "http://demo.hubapi.com/contact/", 
            pageName: "Contact Us", 
            redirectUrl: "http://demo.hubapi.com/thank-you/" 
          };
    
          let parametros = {
            firstname: this.form.controls['firstname'].value,
            lastname: this.form.controls['lastname'].value,
            mobilephone: this.form.controls['mobilephone'].value,
            email: this.form.controls['email'].value,
            city: this.form.controls['city'].value,
            phone: this.form.controls['phone'].value,
            hs_context: parametros_HS
          }
    
          
          this.hubspot.sendToHubspot(parametros).subscribe(res=>{
            console.log('--->>>',res);
          });
        }

  }
}
