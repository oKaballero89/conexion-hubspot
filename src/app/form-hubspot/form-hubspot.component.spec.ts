import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormHubspotComponent } from './form-hubspot.component';

describe('FormHubspotComponent', () => {
  let component: FormHubspotComponent;
  let fixture: ComponentFixture<FormHubspotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormHubspotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormHubspotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
