import { FormHubspotComponent } from './form-hubspot/form-hubspot.component';
import { DetalleContactoComponent } from './detalle-contacto/detalle-contacto.component';
import { ContactoComponent } from './contacto/contacto.component';
import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  //{ path: '', redirectTo: '/', pathMatch: 'full' },
  { path: '', redirectTo: 'contacto', pathMatch: 'full' },
  { path: 'contacto', component: ContactoComponent },
  { path: 'contacto/:email', component: ContactoComponent },

  { path: 'detail', component: DetalleContactoComponent },
  { path: 'detail/:email', component: DetalleContactoComponent },

  { path: 'formHub', component: FormHubspotComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
