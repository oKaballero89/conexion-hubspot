import { RealDataService } from './../service/real-data.service';
import { HubspotCnnService } from './../service/hubspot-cnn.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { FormControl, FormGroup, FormBuilder, Validators, FormGroupDirective, NgForm } from '@angular/forms';


@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {
  collection = [];
  //myGroup: FormGroup;
  //email:FormControl;
  email:string;
  detailList:any;

  
  
  constructor( private srvCnn: HubspotCnnService, private route: ActivatedRoute, private router: Router, public helper: RealDataService) {
    this.route.params.subscribe(params => {
      
      if(params.email){        
        this.requestApi(params.email);
      }
      

      // In a real app: dispatch action to load the details here.
   });
  }

  p = 1;

  ngOnInit() {
    
   

  }

  requestApi(email){
    this.email = email;
    this.srvCnn.getInfo(email).subscribe(res=>{
      
      if(res['events']){
        this.collection  =  this.helper.extractContent(res['events']);
        this.detalleForms();
        //
        //console.log('Datos muestra ',this.collection)
      }else{
        this.collection  =  [];
      }
      //this.collection  = res['events'];
    });
  }

  parseDate(timestampDate){
    return moment(timestampDate).format("MM/DD/YYYY h:mm:ss");
  }

  detalleForms(){
    this.srvCnn.getDetail(this.email).subscribe(res=>{
      //console.log('datos form ', res['form-submissions']    );
      if(res['form-submissions']){
        
        //this.detailList = res['form-submissions'];
        this.collection.push( this.helper.extractContentForm(res['form-submissions']) );
      }else{
        this.detailList = [];
      }
    });
  }

 
  


}
