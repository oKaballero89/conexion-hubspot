import { HubspotCnnService } from './../service/hubspot-cnn.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-detalle-contacto',
  templateUrl: './detalle-contacto.component.html',
  styleUrls: ['./detalle-contacto.component.css']
})
export class DetalleContactoComponent implements OnInit {
  email: string;
  detailList:any;
  p=1;
  constructor(private srvCnn: HubspotCnnService, private route: ActivatedRoute, private router: Router) { 
    this.route.params.subscribe(params => {
      
      if(params.email){    
        this.email = params.email;    
        this.getData(params.email);
      }
      // In a real app: dispatch action to load the details here.
   });
  }

  ngOnInit() {
  }

  getData(email){
    this.srvCnn.getDetail(email).subscribe(res=>{
      //console.log('datos form ', res['form-submissions']    );
      if(res['form-submissions']){
        this.detailList=res['form-submissions'];
        console.log('cosas', this.detailList);
      }else{
        this.detailList = [];
      }
    });

  }

  parseDate(timestampDate){
    return moment(timestampDate).format("MM/DD/YYYY h:mm:ss");
  }
}
