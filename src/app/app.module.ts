import { RealDataService } from './service/real-data.service';
import { HubspotCnnService } from './service/hubspot-cnn.service';
import {  HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { ContactoComponent } from './contacto/contacto.component';
import { Routes, RouterModule  } from '@angular/router';
import {NgxPaginationModule} from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetalleContactoComponent } from './detalle-contacto/detalle-contacto.component';
import { FormHubspotComponent } from './form-hubspot/form-hubspot.component';

@NgModule({
  declarations: [
    AppComponent,
    ContactoComponent,
    DetalleContactoComponent,
    FormHubspotComponent
  ],
  imports: [
    BrowserModule,
    NgxPaginationModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule

  ],
  providers: [HubspotCnnService, RealDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
