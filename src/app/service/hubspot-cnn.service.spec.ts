import { TestBed, inject } from '@angular/core/testing';

import { HubspotCnnService } from './hubspot-cnn.service';

describe('HubspotCnnService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HubspotCnnService]
    });
  });

  it('should be created', inject([HubspotCnnService], (service: HubspotCnnService) => {
    expect(service).toBeTruthy();
  }));
});
