import { Injectable } from '@angular/core';
//import { Http, Headers, Response} from '@angular/http';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import {Observable} from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class HubspotCnnService {
  Hero=[];
  httpOptions;

  constructor( private http: HttpClient) {  }

  private logResponse(res: Response){
    // console.log(res);
   }
 
   private extractData(res: Response){    
     return res.json();
   }
 
   private extractDataPlain(res: Response){    
     return res;
   }


   getInfo(email){
     let url  = 'https://test.devmx.com.mx/info.php?email='+email+'&from_req=consult_email';
    //let url = 'http://api.hubapi.com/email/public/v1/events?hapikey=b1bcb3ee-267b-4bbf-9d61-c664078cf118&recipient=ing.omarkaballero@gmail.com&limit=1000';
     return this.http.get(url);
    
  }

  getDetail(email){
    let url  = 'https://test.devmx.com.mx/info.php?email='+email+'&from_req=detail_email';
   //let url = 'http://api.hubapi.com/email/public/v1/events?hapikey=b1bcb3ee-267b-4bbf-9d61-c664078cf118&recipient=ing.omarkaballero@gmail.com&limit=1000';
    return this.http.get(url);  
  }
  
  getCookie() {
    let name = "hubspotutk=";
    let ca = document.cookie.split(';');

    for(let i = 0; i <ca.length; i++) {
      let c = ca[i];
      
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
  }

  sendToHubspot(data){
    console.log('llego al punto de enviar', data);
    // let cabecera = new Headers({ 'token': params.token});
    // cabecera.append('Content-Type', 'application/x-www-form-urlencoded');

    let param_options = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
    
    //let param_options = new RequestOptions({headers: cabecera});
    //let url = 'https://api.vuala.io/api/admin_app/contacto-hubspot';
    let url = 'https://test.devmx.com.mx/cnn_hubspot.php';
    return this.http.post(url, data, {headers: param_options });    
  }

 
}
