import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RealDataService {

  listaTemp=[];
  listaReal=[];
  constructor() { }

  

  extractContent(listData){    
    let listaRetorno=[];

    let newSendBy=null;

    for (let i = 0; i < listData.length; i++ ){    
      
      this.validaIsUniqueCorreo(listData[i]);

      if(listData[i].v|| listData[i].type == 'SENT'  ){
        if(listaRetorno.length <3){
          listaRetorno.push(listData[i]);
        }        
      }
    }

    return this.listaTemp;
  }
  
  
  validaIsUniqueCorreo(item){

    let IsUnique = true;
    let itemCompleteInfo=[];
    if(this.listaTemp.length > 0){
      for( let i = 0; i <  this.listaTemp.length; i++ ){
        if(item["sentBy"]['id'] == this.listaTemp[i].sendById) {
          IsUnique = false;
          
          if(item["type"]  == 'SENT'   ){            
            this.completeArray(item,  i  );
          }
        }
          
        ///////////////////////////////////////////
      }

      if(IsUnique){
        this.addToList(item, itemCompleteInfo );
        console.log('Es unico');
      }else{
        
      }


    }else{
      //console.log('entro por que esta vaia la lista');
      ///////////////////////////////////////////
      this.listaTemp.push({ tipo: 'Correo', 
        fecha_reg: item['created'],
        titulo: (item["subject"])? item["subject"]: null  ,
        result: item["type"],
        public_url: '',
        sendById: item["sentBy"]['id']
      });
      ///////////////////////////////////////////
    }
  }

  addToList(item, complete_info){
    
    this.listaTemp.push({ tipo: 'Correo', 
      fecha_reg: item['created'],
      titulo: (complete_info["subject"])? complete_info["subject"]: null  ,
      result: item["type"],
      public_url: '',
      sendById: item["sentBy"]['id']
    });
  }

  completeArray(item, position){
    let asunto;
    if(item.subject){
      asunto = item.subject;
    }else{
      asunto = 'N/A';
    }
    this.listaTemp[position].titulo = asunto;
  }

  extractContentForm(listData){
    
    for(let i = 0; i < listData.length; i++ ){
      this.listaTemp.push({ tipo: 'Formulario', 
        fecha_reg: listData[i]['timestamp'],
        titulo: listData[i]['title'], //(listData[i]["page-title"])? listData[i]["page-title"]: null  ,
        result: null,
        public_url: listData[i]['page-url'],
        sendById: null
      });
    }
    
  }

}


